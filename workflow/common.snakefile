rule minimap2_align:
    input:
        fq="results/fastq/{sample}.fq",
        ref=config["ref_genome"]
    output: "results/bam/{sample}.bam"
    wildcard_constraints:
        sample="((?!_sizeSelected).)*" # avoiding ambiguity with in_silico_size_selection rule
    threads: 12
    resources: mem_mb=32768
    conda: "envs/minimap.yaml"
    shell: "minimap2 --MD -L -t 9 -ax map-ont -y {input.ref} {input.fq} | samtools view -bS - | samtools sort - > {output}"

rule in_silico_size_selection:
    input:
      bam="results/bam/{sample}.bam",
      bai="results/bam/{sample}.bam.bai"
    output: "results/bam/{sample}_sizeSelected_{min}bp_{max}bp.bam"
    threads: 12
    resources: mem_mb=32768
    conda: "envs/minimap.yaml"
    shell: "alignmentSieve --minFragmentLength {wildcards.min} --maxFragmentLength {wildcards.max} -b {input.bam} -o {output} -p {threads}"

rule indexBAM:
    input: "{sample}.bam"
    output: "{sample}.bam.bai"
    conda: "envs/minimap.yaml"
    shell: "samtools index {input}"

rule coverage_wig:
    input:
        bam = "results/bam/{sample}.bam",
        bai = "results/bam/{sample}.bam.bai"
    output: "results/igv/{sample}.wig"
    threads: 1
    conda: "envs/igvtools.yaml"
    shell: "igvtools count -w 10000 {input.bam} {output} hg19"

rule coverage_mosdepth:
    input:
        bam = "results/bam/{sample}.bam",
        bai = "results/bam/{sample}.bam.bai"
    output:
        "results/stats/{sample}.mosdepth.summary.txt",
        "results/stats/{sample}.regions.bed.gz"
    threads: 1
    conda: "envs/mosdepth.yaml"
    shell: "mosdepth -n --fast-mode --by 10000 results/stats/{wildcards.sample} {input.bam}"

rule QC_nanostat:
    input:
        bam = "results/bam/{sample}.bam",
        bai = "results/bam/{sample}.bam.bai"
    output: "results/stats/{sample}.nanostat.txt"
    threads: 12
    conda: "envs/qc.yaml"
    shell: "NanoStat -t {threads} --bam {input.bam} --no_supplementary > {output}"

### CNV analysis using ichorCNA

rule read_counter:
  input:
    bam = "results/bam/{samples}.bam",
    bai = "results/bam/{samples}.bam.bai"
  output: "results/ichorCNA/{samples}.bin.wig"
  conda: "envs/ichorCNA.yaml"
  shell: "readCounter {input.bam} -w 1000000 -c $(echo chr{{1..22}} | sed 's/ /,/g'),chrX,chrY -q 20 > {output}" # bin size 1 Mb, min mapQ > 20

rule ichorCNA:
  input: "results/ichorCNA/{sample}.bin.wig",
  output:
    pdf="results/ichorCNA/{sample}_genomeWide.pdf",
    seg="results/ichorCNA/{sample}.seg",
    txt="results/ichorCNA/{sample}.params.txt",
    dir=directory("results/ichorCNA_detailed/{sample}")
  conda: "envs/ichorCNA.yaml"
  shell: """mkdir -p {output.dir} && \
            R -e "if (file.exists('static/ichorCNA')==FALSE) system(paste0('mkdir -p static/ichorCNA && cp ', find.package('ichorCNA'), '/extdata/* static/ichorCNA/'))" && \
            runIchorCNA.R \
              --id {wildcards.sample} \
              --WIG {input} \
              --gcWig static/ichorCNA/gc_hg19_1000kb.wig \
              --mapWig static/ichorCNA/map_hg19_1000kb.wig \
              --normalPanel static/ichorCNA/HD_ULP_PoN_1Mb_median_normAutosome_mapScoreFiltered_median.rds \
              --genomeStyle UCSC \
              --estimateNormal TRUE \
              --estimatePloidy TRUE \
              --estimateScPrevalence FALSE \
              --minMapScore 0.75 \
              --normal  "c(0.5,0.6,0.7,0.8,0.9,0.95,0.99)" \
              --ploidy "c(2,3)" \
              --txnStrength 10000 \
              --txnE 0.9999 \
              --plotYLim "c(-2,4)" \
              --outDir {output.dir} && \
            cp {output.dir}/{wildcards.sample}/{wildcards.sample}_genomeWide.pdf {output.pdf} && \
            cp {output.dir}/{wildcards.sample}.seg {output.seg} && \
            cp {output.dir}/{wildcards.sample}.params.txt {output.txt}
         """
