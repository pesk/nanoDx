rule modbam_to_bedMethyl:
  input:
    bam = "results/bam/{sample}.bam",
    bai = "results/bam/{sample}.bam.bai",
    ref = config["ref_genome"]
  output: "results/bedMethyl/{sample}.CpG.bed.gz"
  conda: "envs/minimap.yaml"
  resources: mem_mb=32768
  threads: 12
  shell: "modkit pileup {input.bam} - --ref {input.ref} --preset traditional --only-tabs  --edge-filter 0,27 --threads {threads} | gzip > {output}"

rule annotate_bedMethyl:
    input:
        bed="results/bedMethyl/{sample}.CpG.bed.gz",
        mapping=Path(workflow.basedir).parent / "static/450K_hg19.bed"
    output: "results/bedMethyl/{sample}.CpG.450K.bed"
    resources: mem_mb=16384
    conda: "envs/minimap.yaml"
    shell: "zcat {input.bed} | cut -f1-11 | bedtools intersect -a - -b {input.mapping} -wa -wb | awk -v OFS='\t' '$4=$15' | cut -f1-11 > {output}"

rule plot_tSNE:
    input:
        bed="results/bedMethyl/{sample}.CpG.450K.bed",
        trainingset=config["trainingset_dir"] + "/{trainingset}.h5",
        colorMap=lambda wildcards: Path(workflow.basedir).parent / f"static/colorMap_{wildcards.trainingset}.txt"
    output:
        pdf="results/plots/{sample}-tSNE-{trainingset}.pdf",
        html="results/plots/{sample}-tSNE-{trainingset}.html"
    params:
        dim_reduction_method = config["dim_reduction_method"] if 'dim_reduction_method' in config.keys() else 'tsne',
        tsne_pca_dim = config["tsne_pca_dim"] if 'tsne_pca_dim' in config.keys() else 94,
        tsne_perplexity = config["tsne_perplexity"] if 'tsne_perplexity' in config.keys() else 30,
        tsne_max_iter = config["tsne_max_iter"] if 'tsne_max_iter' in config.keys() else 2500,
        umap_n_neighbours = config["umap_n_neighbours"] if 'umap_n_neighbours' in config.keys() else 10,
        umap_min_dist = config["umap_min_dist"] if 'umap_min_dist' in config.keys() else 0.5,
        umap_pca_dim = config.get("umap_pca_dim", 100)
    conda: "envs/tSNE.yaml"
    threads: 4
    resources: mem_mb=64768
    script: "scripts/plot_tSNE.R"

rule NN_classifier:
    input:
        bed = "results/bedMethyl/{sample}.CpG.450K.bed",
        model = config["trainingset_dir"] + "/{trainingset}_NN.pkl"
    output:
        txt="results/classification/{sample}-votes-NN-{trainingset}.txt",
        votes="results/classification/{sample}-votes-NN-{trainingset}.tsv"
    conda: "envs/NN_model.yaml"
    threads: 4
    resources: mem_mb=16384
    script: "scripts/classify_NN_bedMethyl.py"
